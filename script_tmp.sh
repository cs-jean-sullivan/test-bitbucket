#/bin/bash

#Script para:
#limpar arquivos temporários do diretório /tmp
#limpar arquivos txt do diretório /home/jeansullivan/arquivos
#Gerar dump do MySQL da base: testedatabase no diretório /backup

cd /tmp
rm -rf *.tmp

cd /home/jeansullivan/arquivos
rm -rf *.txt

cd /backup
mysqldump -u root -p xxxxx testedatabase > testedatabase.sql
 
exit
